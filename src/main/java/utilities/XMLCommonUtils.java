package utilities;

import java.io.File;

public class XMLCommonUtils extends JAXBMarshelling {
	
	/**
	 * File name that need to unmarshell
	 * and full class name is the name of class with package
	 * @param fileName
	 * @param fullClassName
	 * @return
	 */
	public Object convertMasterXmlToObject(String fileName, String fullClassName) {
		return getObjectFromSoapMessageFile(fullClassName,
				new File(System.getProperty("user.dir") + File.separator + "src" + File.separator + "main"
						+ File.separator + "resources" + File.separator + "xmlfiles" + File.separator + fileName));
	}

	/**
	 * Object need to be converted and Output file name
	 * @param obj
	 * @param fileName
	 */
	public void convertObjectsToXMLFile(Object obj,String fileName) {
		jaxbObjectToXML2(obj, System.getProperty("user.dir") + File.separator + "src" + File.separator + "main"
				+ File.separator + "resources" + File.separator + "xmlfiles" + File.separator + fileName);
	}
}
