package utilities;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class JAXBMarshelling {

	public JAXBMarshelling() {
		
	}

	public Object getObjectFromSoapMessageNode(String className, Node msg) {
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(createInstanceOfClass(className).getClass());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
			
		}
		Unmarshaller m = null;
		try {
			m = context.createUnmarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		try {
			return m.unmarshal(msg);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return m;
	}

	public Object getObjectFromSoapMessageFile(String className, File file) {
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(createInstanceOfClass(className).getClass());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		Unmarshaller m = null;
		try {
			m = context.createUnmarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		try {
			return m.unmarshal(file);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return m;
	}

	public Object getObjectFromSoapMessageNode(String className, Document msg) {
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(createInstanceOfClass(className).getClass());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		Unmarshaller m = null;
		try {
			m = context.createUnmarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		try {
			return m.unmarshal(msg);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return m;
	}

	private Object xmlToObject(@SuppressWarnings("rawtypes") Class clazz, String fileName) throws JAXBException {
		Unmarshaller un = getJAXBUnMarsheller(clazz);
		return un.unmarshal(new File(fileName));
	}

	public Object jaxbXMLToObject(String className, String FILE_NAME) {
		try {
			Object obj = createInstanceOfClass(className);
			return xmlToObject(obj.getClass(), FILE_NAME);
		} catch (JAXBException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public void jaxbObjectToXML(String className, String filePath) {
		try {
			Object obj = createInstanceOfClass(className);
			Marshaller m = getJAXBMarsheller(createInstanceOfClass(className).getClass());
			m.marshal(obj, System.out);

			// Write to File
			m.marshal(obj, new File(filePath));
		} catch (JAXBException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void jaxbObjectToXML2(Object obj, String filePath) {
		try {
			//Object obj = createInstanceOfClass(className);
			Marshaller m = getJAXBMarsheller(obj.getClass());
			m.marshal(obj, System.out);

			// Write to File
			m.marshal(obj, new File(filePath));
		} catch (JAXBException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	

	@SuppressWarnings("rawtypes")
	private Marshaller getJAXBMarsheller(Class clazz) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(clazz);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		return m;
	}

	@SuppressWarnings("rawtypes")
	private Unmarshaller getJAXBUnMarsheller(Class clazz) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(clazz);
		Unmarshaller m = context.createUnmarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		return m;
	}

	@SuppressWarnings("rawtypes")
	public Object createInstanceOfClass(String className) {
		Class classTemp = null;
		try {
			classTemp = Class.forName(className);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.err.println(e1.getMessage());
			e1.printStackTrace();
		}
		Object obj = null;
		try {
			obj = classTemp.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return obj;
	}
	
}
