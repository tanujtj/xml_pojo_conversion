package mainclass;

import com.pojos.TranscationResponse;
import com.pojos.TranscationResponse.CstmrCdtTrfInitn;

import utilities.XMLCommonUtils;

public class MainClass {

	public static void main(String...a) {
		XMLCommonUtils utils = new XMLCommonUtils();
		/**
		 * Below lines convert the master test xml to corresponding java pojo object
		 * The corresponding java objects are created via its .xsd file
		 */
		TranscationResponse transcationResponse = (TranscationResponse)  utils.convertMasterXmlToObject("master_test.xml", "com.pojos.ObjectFactory");
		System.out.println(transcationResponse);
		/**
		 * Below is the example of getting any attribute
		 */
		System.out.println(transcationResponse.getCstmrCdtTrfInitn().getGrpHdr().getMsgId());
		
		/**
		 * Below is the example of setting the value of any node
		 */
		CstmrCdtTrfInitn CstmrCdt = transcationResponse.getCstmrCdtTrfInitn();
		CstmrCdt.getGrpHdr().setMsgId("Testing: This to check the dynamic binding");;
		CstmrCdt.getGrpHdr().setCreDtTm("HH MM SS hello : Covid making me angry");
		CstmrCdt.getGrpHdr().getInitgPty().getId().getOrgId().getOthr().setId("This is other id");
		transcationResponse.setCstmrCdtTrfInitn(CstmrCdt);
		
		/**
		 * Second way dont hold reference just set values
		 */
		transcationResponse.getCstmrCdtTrfInitn().getPmtInf().setPmtInfId("This is PMT Id");
		transcationResponse.getCstmrCdtTrfInitn().getPmtInf().getDbtrAcct().setCcy("2050");
		transcationResponse.getCstmrCdtTrfInitn().getPmtInf().getDbtrAcct().getId().getOthr().setId("10712381273483126");;
		// this line to verify value set at object level
		System.out.println(transcationResponse.getCstmrCdtTrfInitn().getGrpHdr().getMsgId());
		/**
		 * Convert the object into file
		 */
		utils.convertObjectsToXMLFile(transcationResponse,"output_test.xml");
	}
}
